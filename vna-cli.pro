TEMPLATE = app
TARGET = vna-cli
QT += serialport

DEFINES += QT_DEPRECATED_WARNINGS

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += vnaserial.h
SOURCES += main.cpp vnaserial.cpp

