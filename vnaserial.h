#ifndef VNASERIAL_H
#define VNASERIAL_H

#include <QSerialPort>
#include <QVector>
/*
 * VnaSerial: functionality to read the output from a miniVNA Tiny over a serial (USB) interface
 * Format of the request (each line should be terminated by a carriage return \r):
 * 10 //gets the temperature
 * 8 //gets something else
 * 7  // get S-parameter  (7 reflection / 6 transmission)
 * 1000000 //start frequency / 10
 * 299999938 //stop frequncy / 10
 * 994 // number of samples
 * 7
 * 0
 * 0
 * 1
 * 0
 *
 * Output from the VNA: 1 S-parameter (S11 or S21 depending on mode) provided in total 4 x 3 bytes per frequency: 
 * 1: real part 2: imaginary part 3: copy (negated)  of real part, 4: copy (negated) of imaginary part
 * 24 bit integers
 */

struct SParameters {
    unsigned int frequency;
    int values[4]; //structure: real part, imag part, -real part, -imag part
    QString toString() const {
        QString allValuesAsString = QString::number(frequency) + ", ";
        for (int i = 0; i < 4; ++i) {
            allValuesAsString += QString::number(values[i]) + ", ";
        }
        return allValuesAsString;
    }
};

class VnaSerial {
public:
    enum MeasurementMode {
        REFLECTION,
        TRANSMISSION
    };
    int initVna(QString deviceName);
    /*
     * Return the requestedNbSamples S-parameter (S11 or S21 in reflection / transmission mode respectivey) for all the frequencies
     * between startFrequency and stopFrequency, both included
     */
    QVector<SParameters> doMeasure(MeasurementMode mode,
                                   unsigned int startFrequency, unsigned int stopFrequency, unsigned int requestedNbSamples);
    /*
     * Returns the temperature of the VNA in deci-°C (e.g. 446 indicates 44.6 °C)
     */
    int temperature();
private:
    void sendParam(unsigned int x);
    static QVector<SParameters> extractSParametersFromSerialResponse(const QByteArray& serialResponse,
                                                                     unsigned int startFrequency, unsigned int stopFrequency);
    QSerialPort serial;
};

#endif
