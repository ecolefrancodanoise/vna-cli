#include <QCoreApplication>
#include <QCommandLineParser>
#include "vnaserial.h"
#include <iostream>

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QCommandLineParser parser;
    parser.setApplicationDescription("Command line tool to extract S-parameters (S11 and S21) from a miniVNA Tiny");
    parser.addHelpOption();
    parser.addOptions({
        {   "m",
            QCoreApplication::translate("main", "Measurement mode, R: reflection, T: transmission, default R"),
            QCoreApplication::translate("main", "mode")
        },
        {   "p",
            QCoreApplication::translate("main", "Serial port, default /dev/ttyUSB0"),
            QCoreApplication::translate("main", "Port name")
        },
        {   "start",
            QCoreApplication::translate("main", "Start frequency (daHz), default 1000000"),
            QCoreApplication::translate("main", "frequency")
        },
        {   "stop",
            QCoreApplication::translate("main", "Stop frequency (daHz), default 300000000"),
            QCoreApplication::translate("main", "frequency")
        },
        {   "samples",
            QCoreApplication::translate("main", "Number of samples, default 1001"),
            QCoreApplication::translate("main", "samples")
        },
        {   "t",
            QCoreApplication::translate("main", "Read temperature")
        },
    });
    parser.process(app);
    VnaSerial vna;
    if (parser.isSet("p")) {
        vna.initVna(parser.value("p"));
    } else {
        vna.initVna("/dev/ttyUSB0");
    }
    VnaSerial::MeasurementMode measurementMode = VnaSerial::REFLECTION;
    if (parser.isSet("m") && parser.value("m") == 'T') {
        measurementMode = VnaSerial::TRANSMISSION;
    }
    if (parser.isSet("t")) {
        std::cout << vna.temperature() << std::endl;
    } else {
        const int startFrequency = parser.isSet("start") ? parser.value("start").toInt() : 1000000;
        const int stopFrequency = parser.isSet("stop") ? parser.value("stop").toInt() : 300000000;
        const int nbSamples = parser.isSet("samples") ? parser.value("samples").toInt() : 1001;
        QVector<SParameters> sParameterForAllFrequencies = vna.doMeasure(measurementMode, startFrequency, stopFrequency, nbSamples);
        for(const SParameters& sParameter : sParameterForAllFrequencies) {
            std::cout << sParameter.toString().toStdString() << std::endl;
        }
    }
    return 0;
}
