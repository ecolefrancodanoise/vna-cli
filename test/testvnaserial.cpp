#include <QtTest/QtTest>
#include "../vnaserial.h"

#define SERIALPORT "/dev/ttyUSB0"

class TestVnaSerial: public QObject {
    Q_OBJECT
private slots:
    void reflectionTest();
    void transmissionTest();
    void temperatureTest();
};

void TestVnaSerial::reflectionTest() {
    VnaSerial vna;
    vna.initVna(SERIALPORT);
    QVector<SParameters> sParameterForAllFrequencies = vna.doMeasure(VnaSerial::REFLECTION, 1000000, 299999938, 10);
    QCOMPARE(sParameterForAllFrequencies.length(), 10);
}

void TestVnaSerial::transmissionTest() {
    VnaSerial vna;
    vna.initVna(SERIALPORT);
    QVector<SParameters> sParameterForAllFrequencies = vna.doMeasure(VnaSerial::TRANSMISSION, 1000000, 299999938, 10);
    QCOMPARE(sParameterForAllFrequencies.length(), 10);
}

void TestVnaSerial::temperatureTest() {
    VnaSerial vna;
    vna.initVna(SERIALPORT);
    const int temp = vna.temperature();
    QVERIFY2(temp < 600, "Temperature is expected to be below 60°C");
    QVERIFY2(temp > 10, "Temperature is expected to be above 1°C");
}

QTEST_MAIN(TestVnaSerial)
#include "testvnaserial.moc"

