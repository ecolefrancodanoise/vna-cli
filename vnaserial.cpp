#include "vnaserial.h"
#include <QtSerialPort>

#define TIMEOUT1 1000

int VnaSerial::initVna(QString deviceName) {
    serial.setPortName(deviceName);
    if (serial.open(QIODevice::ReadWrite)) {
        if (   serial.setBaudRate(921600)
            && serial.setDataBits(QSerialPort::Data8)
            && serial.setParity(QSerialPort::NoParity)
            && serial.setStopBits(QSerialPort::OneStop)
            && serial.setFlowControl(QSerialPort::NoFlowControl)) {
            return 0;
        } else {
            serial.close();
            return -1;
        }
    }
    return -1;
}

QVector<SParameters> VnaSerial::doMeasure(MeasurementMode mode, unsigned int startFrequency, unsigned int stopFrequency, unsigned int requestedNbSamples) {
    Q_ASSERT(stopFrequency > startFrequency);
    QByteArray responseData;
    switch (mode) {
        case REFLECTION:
            sendParam(7);
            break;
        case TRANSMISSION:
            sendParam(6);
            break;
    }
    sendParam(startFrequency); //start frequency (daHz)
    sendParam(stopFrequency); //stop frequency  (daHz)
    sendParam(requestedNbSamples); //number of samples
    sendParam(7); //Go ? // the first 2 header entries can be read at this point
    sendParam(0); // configuring attenuation?
    sendParam(0); //?
    sendParam(1); //?
    sendParam(0); //?
    if (serial.waitForBytesWritten(TIMEOUT1)){
        while(serial.waitForReadyRead(TIMEOUT1)) {
            responseData += serial.readAll();
        }
    }
    serial.close();
    Q_ASSERT(responseData.length() / 12 == requestedNbSamples);
    return extractSParametersFromSerialResponse(responseData, startFrequency, stopFrequency);
}

void VnaSerial::sendParam(unsigned int x) {
    QByteArray s = QByteArray::number(x);
    serial.write(s + "\r");
}

QVector<SParameters> VnaSerial::extractSParametersFromSerialResponse(const QByteArray& serialResponse, unsigned int startFrequency, unsigned int stopFrequency) {
    int nbResponseBytes = serialResponse.length();
    Q_ASSERT(nbResponseBytes %12 == 0);
    unsigned int nbSamples = nbResponseBytes / 12;
    QVector<SParameters> sParameters(nbSamples);
    unsigned int frequencyStep = (stopFrequency - startFrequency) / (nbSamples - 1);
    for (unsigned int i = 0; i < nbSamples; ++i) {
        sParameters[i].frequency = (startFrequency + frequencyStep * i) * 10; //Note: conversion to from daHz to Hz here.
        for (unsigned int j = 0; j < 4; ++j) {
            sParameters[i].values[j] = (quint8)serialResponse[i*12 + j*3]
                                     + (quint8)serialResponse[i*12 + j*3 + 1] * 256
                                     + (quint8)serialResponse[i*12 + j*3 + 2] * 256 * 256 - (256 * 256 *256) / 2;
        }
    }
    return sParameters;
}

int VnaSerial::temperature() {
    sendParam(10); //opcode to get temperature
    serial.waitForReadyRead(TIMEOUT1);
    QByteArray temperatureReading = serial.readAll();
    Q_ASSERT(temperatureReading.length() == 2);
    return (quint8)temperatureReading[0] + (quint8)temperatureReading[1] * 256;

}
